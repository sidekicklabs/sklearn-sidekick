{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Boston example\n",
    "\n",
    "In this example we will define, tune and evaluate a regression algorithm to predict house prices, using the boston dataset. \n",
    "\n",
    "First, let's do the boring stuff of importing dependencies."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sklearnsk as sksk\n",
    "\n",
    "import sklearn\n",
    "from sklearn import datasets\n",
    "\n",
    "import os\n",
    "import scipy\n",
    "\n",
    "import pandas as pd"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's also import the dataset (see [sklearn documentation](http://scikit-learn.org/stable/datasets/index.html#datasets) for more details). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "boston = datasets.load_boston()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And let's define some helper functions for printing out nice result tables"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def print_score_table(results):\n",
    "    \n",
    "    scoreTable = pd.DataFrame.from_dict(results.scores).transpose()\n",
    "    print(scoreTable)\n",
    "    \n",
    "def print_pairwise_comparisons(results):\n",
    "    \n",
    "    print(\"pairwise comparisons by {}\".format(results.pairwise_comparison_metric))\n",
    "    print()\n",
    "    \n",
    "    comparisonsTable = pd.DataFrame.from_dict(results.pairwise_comparisons)\n",
    "    print(comparisonsTable)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Create our regression algorithm\n",
    "\n",
    "We implement the ConfigurableAlgorithm abstract class, which forces us to construct our algorithm in \n",
    "a way that will make it easy to tune and evaluate later\n",
    "\n",
    "The important thing is that everything tunable and tweakable about our algorithm is specified in a single config\n",
    "dictionary that is passed in when we first instantiate the class. \n",
    "\n",
    "In this example, we can tune:\n",
    "\n",
    "* The algorithm (e.g. SVR, LinearRegression, etc)\n",
    "* Any hyperparameters of that algorithm (e.g. C, gamma, epsilon, etc)\n",
    "* features to use or ignore (this will let us to feature ablation later)\n",
    "* whether features should be normalized or not\n",
    "\n",
    "But in a more complex problem there might be many more things we would want to configure, such as whether to do feature selection, by which method, what number of features to retain, what value of n to use when n-gramming text features, etc etc. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class BostonHousePricePredictor(sksk.ConfigurableAlgorithm):\n",
    "    \n",
    "    def get_default_config(self):\n",
    "        \n",
    "        return {\n",
    "            'algorithm': 'SVR',\n",
    "            'features': boston.feature_names,\n",
    "            'normalise': True\n",
    "        }\n",
    "    \n",
    "    def get_custom_config_param_names(self):\n",
    "        \n",
    "        return ['features', 'normalise']\n",
    "    \n",
    "    def build_feature_vectors(self, items, labelsOrScores=None):\n",
    "        \n",
    "        features = [self.build_feature_vector(item) for item in items]\n",
    "    \n",
    "        if self.config['normalise']:\n",
    "            \n",
    "            if labelsOrScores is not None:\n",
    "                \n",
    "                self.scaler = sklearn.preprocessing.StandardScaler()\n",
    "                features = self.scaler.fit_transform(features)\n",
    "                \n",
    "            else:\n",
    "                features = self.scaler.transform(features)\n",
    "                \n",
    "        return features\n",
    "    \n",
    "    def build_feature_vector(self, item):\n",
    "        \n",
    "        f = []\n",
    "        \n",
    "        for i in range(len(boston.feature_names)):\n",
    "            fname = boston.feature_names[i]\n",
    "            \n",
    "            if fname in self.config['features']:\n",
    "                f.append(item[i])\n",
    "                \n",
    "        return f"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Tune the regression algorithm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.1 Define our tuner\n",
    "\n",
    "We can use either a `GridParameterSearcher` if we only have a few parameters \n",
    "and want to try all combinations exhaustively), or a `RandomParameterSearcher` \n",
    "if we have many parameters and want to try random combinations.\n",
    "\n",
    "In this example we will use the `RandomParameterSearcher`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class BostonTuner(sksk.RandomParameterSearcher):\n",
    "    \n",
    "    def initialize_algorithm(self, config):\n",
    "        \n",
    "        return BostonHousePricePredictor(config)\n",
    "    \n",
    "tuner = BostonTuner(items=boston.data, scores=boston.target)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.2 Define the parameter search space\n",
    "\n",
    "In this example, we are focusing exclusively on the SVR algorithm with an RBF kernel, and simultaneously tuning variables that are specific to our BostonHousePricePredictor (*normalise*) and specific to the SVC algorithm (*C*, *gamma* and *epsilon*). We could set up a similar parameter space for optimizing other algorithms, such as RandomForestRegression, etc. \n",
    "\n",
    "Check out the [sklearn guide](http://scikit-learn.org/stable/modules/grid_search.html#randomized-parameter-search) for more info about setting up this parameter space. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "svr_search_params = {\n",
    "    'algorithm':['SVR'],\n",
    "    'kernel':['rbf'],\n",
    "    'C': scipy.stats.expon(scale=20),\n",
    "    'gamma': scipy.stats.expon(scale=0.01),\n",
    "    'epsilon': scipy.stats.expon(scale=0.1),\n",
    "    'normalise':[True,False]\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.3 Either load or initialize our parameter search record\n",
    "\n",
    "Parameter searches can take a loooong time. This search record stores results to disk, so we can freely start and stop our search without loosing progress."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if not os.path.exists('tuning'):\n",
    "    os.mkdir('tuning')\n",
    "\n",
    "svr_search_record = sksk.ParameterSearchRecord('tuning/boston_svr_tuning.pkl')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.4 Run the parameter search\n",
    "\n",
    "In this example we have a budget of 1000, meaning that we will try 1000 random combinations of parameters, and keep the best one"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bestConf, bestScore = tuner.do_search(svr_search_params, svr_search_record, budget=1000)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('best score is {}, obtained with:'.format(bestScore))\n",
    "\n",
    "for (key, val) in bestConf.items():\n",
    "    print(\"  {} = {}\".format(key, val))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.5 Visualize parameter search\n",
    "\n",
    "It is often useful to visualize a parameter search, to get clues about how we might refine and do a followup search. It is common to start with a broad search and then narrow things down. \n",
    "\n",
    "We can use the `ParameterSearchVisualizer` to create heatmaps of how the gridsearch went.\n",
    "\n",
    "In this example we plot `gamma` and `C` while freezing the other variables. \n",
    "\n",
    "Visualization is a bit trickier with the random search, since we don't have our values arranged in a nice grid. Instead we make a scatter plot of all values that were checked (the black dots in the figure) and then interpolate a contour map from them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "visualizer = sksk.ParameterSearchVisualizer()\n",
    "\n",
    "visualizer.visualize_random_search(\n",
    "    svr_search_record, \n",
    "    x_axis='gamma', \n",
    "    y_axis='C', \n",
    "    filter_by={'normalise':False, 'epsilon': [0.2, 0.3]},\n",
    "    figsize=(8, 6)\n",
    ")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The best scores happen with values of C between 10 and 40, and values of gamma less than 0.005. We only have a samples (dots) that explore this area, so we can probably focus in and do a lot better.\n",
    "\n",
    "So let's do just that! "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "svr_search_record_2 = sksk.ParameterSearchRecord('tuning/boston_svr_tuning2.pkl')\n",
    "\n",
    "svr_search_params_2 = {\n",
    "    'algorithm':['SVR'],\n",
    "    'kernel':['rbf'],\n",
    "    'C': scipy.stats.norm(loc=25, scale=5),\n",
    "    'gamma': scipy.stats.expon(scale=0.05),\n",
    "    'epsilon': [0.025],\n",
    "    'normalise':[True]\n",
    "}\n",
    "\n",
    "bestConf, bestScore = tuner.do_search(svr_search_params_2, svr_search_record_2, budget=1000)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('best score is {}, obtained with:'.format(bestScore))\n",
    "\n",
    "for (key, val) in bestConf.items():\n",
    "    print(\"  {} = {}\".format(key, val))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That seems to have worked, or at-least we got another 1.5% ish improvement"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "visualizer.visualize_random_search(\n",
    "    svr_search_record_2, \n",
    "    x_axis='gamma', \n",
    "    y_axis='C',\n",
    "    figsize=(8, 6)\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And we have a lot of samples (dots) all focused in on the best area, so it seems unlikely we can do much better"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Evaluate the algorithm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3.1 Define our evaluator\n",
    "\n",
    "We need to define our evaluator class, in much the same way as we defined our parameter searcher"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class BostonEvaluator(sksk.Evaluator):\n",
    "    \n",
    "    def initialize_algorithm(self, config):\n",
    "        \n",
    "        return BostonHousePricePredictor(config)\n",
    "    \n",
    "evaluator = BostonEvaluator(items=boston.data, scores=boston.target)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3.2 Evaluate our algorithm\n",
    "\n",
    "The evaluator takes an array of configurations, and will print out how the algorithm performs with each configuration. In this case we will compare the our tuned algorithm (using the best params from our param search above) with the untuned one (using just default params). The evaluator also automatically adds a random baseline as an extra point of comparison."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "configs = []\n",
    "configs.append(sksk.merge_configs({'id':'tuned'}, bestConf))\n",
    "configs.append({'id':'untuned'})\n",
    "\n",
    "results = evaluator.evaluate(configs)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print_score_table(results)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The results are based on cross-fold validation, which gives slightly different results each time you run it. This lets us also provide pairwise t-tests to check whether the differences are statistically significant."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print_pairwise_comparisons(results)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we had more data, we would ideally have a proper training and testing split, since we have almost certianly overfitted to the data with all of our tuning experiments. The evaluator has a seperate method `evaluate_with_testset()` for this purpose."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3.3 Perform further evaluations and analysis\n",
    "\n",
    "This same process of feeding different configurations into the evaluator can be used for many other evaluations as well. \n",
    "\n",
    "For example, which regression algorithm is better for this problem (ignoring tuning for now)?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "configs = [\n",
    "    {'id':'SVM', 'algorithm':'SVR'},\n",
    "    {'id':'RandomForest', 'algorithm':'RandomForestRegressor'},\n",
    "    {'id':'Linear', 'algorithm':'LinearRegression'}\n",
    "]\n",
    "\n",
    "results = evaluator.evaluate(configs)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print_score_table(results)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Untuned random forest is doing almost as well as fully tuned SVR. If this were more than a toy example, it would be well worth going back to step 2 and tuning RandomForest instead. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can do feature analysis and ablation in exactly the same way..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "configs = []\n",
    "\n",
    "#add a config that uses all features, merging in the best config found during tuning\n",
    "configs.append(sksk.merge_configs({'id':'all features'}, bestConf))\n",
    "\n",
    "#add configs that exclude each feature, one at a time\n",
    "for feature in boston.feature_names:\n",
    "    \n",
    "    config = {\n",
    "        'id': 'without {}'.format(feature),\n",
    "        'features': [f for f in boston.feature_names if f != feature]\n",
    "    }\n",
    "    #again, merge in the best config found during training\n",
    "    config = sksk.merge_configs(config, bestConf)\n",
    "    configs.append(config)\n",
    "    \n",
    "results = evaluator.evaluate(configs)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print_score_table(results)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So, the algorithm hurts most when RM (number of rooms) and LSTAT (% lower status of the population) are removed. That means these are our strongest features. \n",
    "\n",
    "It gets slightly better when we remove ZN (proportion of residential land zoned for lots over 25,000 sq.ft.) and CHAS (whether lot is adjacent to river), indicating that these aren't useful features, or at least that they provide . \n",
    "\n",
    "We would want to double-check the pairwise comparisons to see if these differences are significant."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "language_info": {
   "name": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
