{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Iris example\n",
    "\n",
    "In this example we will define, tune and evaluate a classificatin algorithm to identify flowers, using the iris dataset.\n",
    "\n",
    "First, let's do the boring stuff of importing dependencies."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sklearnsk as sksk\n",
    "\n",
    "import sklearn\n",
    "from sklearn import datasets\n",
    "\n",
    "import os\n",
    "import scipy\n",
    "\n",
    "import pandas as pd"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Load the iris dataset "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data = datasets.load_iris()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Some convenience functions for displaying tables"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def print_score_table(results):\n",
    "    scoreTable = pd.DataFrame.from_dict(results.scores).transpose()\n",
    "    print(scoreTable)\n",
    "    \n",
    "def print_pairwise_comparisons(results):\n",
    "    \n",
    "    print(\"pairwise comparisons by {}\".format(results.pairwise_comparison_metric))\n",
    "    print()\n",
    "    \n",
    "    comparisonsTable = pd.DataFrame.from_dict(results.pairwise_comparisons)\n",
    "    print(comparisonsTable)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Create our classification algorithm\n",
    "\n",
    "We implement the ConfigurableAlgorithm abstract class, which forces us to construct our algorithm in \n",
    "a way that will make it easy to tune and evaluate later\n",
    "\n",
    "The important thing is that everything tunable and tweakable about our algorithm is specified in a single config\n",
    "dictionary that is passed in when we first instantiate the class. \n",
    "\n",
    "In this example, we can tune:\n",
    "\n",
    "* The algorithm (e.g. SVC, RandomForest, etc)\n",
    "* Any hyperparameters of that algorithm (e.g. C, gamma, max_depth)\n",
    "* features to use or ignore (this will let us to feature ablation later)\n",
    "* whether features should be normalized or not\n",
    "\n",
    "But in a more complex problem there might be many more things we would want to configure, such as whether to do feature selection, by which method, what number of features to retain, what value of n to use when n-gramming text features, etc etc. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class IrisClassifier(sksk.ConfigurableAlgorithm):\n",
    "    \n",
    "    def get_default_config(self):\n",
    "        \n",
    "        return {\n",
    "            'algorithm': 'SVC',\n",
    "            'features': data.feature_names,\n",
    "            'normalise': True\n",
    "        }\n",
    "    \n",
    "    def get_custom_config_param_names(self):\n",
    "        \n",
    "        return ['features', 'normalise']\n",
    "    \n",
    "    def build_feature_vectors(self, items, labelsOrScores=None):\n",
    "        \n",
    "        features = [self.build_feature_vector(item) for item in items]\n",
    "    \n",
    "        if self.config['normalise']:\n",
    "            \n",
    "            if labelsOrScores is not None:\n",
    "                \n",
    "                self.scaler = sklearn.preprocessing.StandardScaler()\n",
    "                features = self.scaler.fit_transform(features)\n",
    "                \n",
    "            else:\n",
    "                features = self.scaler.transform(features)\n",
    "                \n",
    "        return features\n",
    "    \n",
    "    def build_feature_vector(self, item):\n",
    "        \n",
    "        f = []\n",
    "        \n",
    "        for i in range(len(data.feature_names)):\n",
    "            fname = data.feature_names[i]\n",
    "            \n",
    "            if fname in self.config['features']:\n",
    "                f.append(item[i])\n",
    "                \n",
    "        return f"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Tune the classification algorithm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.1 Define our tuner\n",
    "\n",
    "We can use either a `GridParameterSearcher` if we only have a few parameters \n",
    "and want to try all combinations exhaustively), or a `RandomParameterSearcher` \n",
    "if we have many parameters and want to try random combinations.\n",
    "\n",
    "In this example we will use the `GridParameterSearcher`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class IrisTuner(sksk.GridParameterSearcher):\n",
    "    \n",
    "    def initialize_algorithm(self, config):\n",
    "        \n",
    "        return IrisClassifier(config)\n",
    "    \n",
    "tuner = IrisTuner(data.data, data.target)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.2 Define the parameter search space\n",
    "\n",
    "In this example, we are focusing exclusively on the SVC algorithm, but will try the linear and rbf kernels. We will simultaneously tune variables that are specific to our IrisClassifier (*normalise*) and specific to the SVC algorithm (*kernel*, *C* and *gamma*). We could set up a similar parameter space for optimizing other algorithms, such as RandomForest, etc. \n",
    "\n",
    "Check out the [sklearn guide](http://scikit-learn.org/stable/modules/grid_search.html) for more info about setting up this parameter space. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "svc_search_params = [\n",
    "    {\n",
    "        'algorithm': ['SVC'],\n",
    "        'C': [1, 5, 10, 25, 50, 100, 500, 1000], \n",
    "        'kernel': ['linear'],\n",
    "        'class_weight': [None, 'balanced'],\n",
    "        'normalise': [True,False]\n",
    "    },\n",
    "    {\n",
    "        'C': [1, 5, 10, 25, 50, 100, 500, 1000], \n",
    "        'gamma': [0.1, 0.01, 0.001, 0.0001, 0.00001], \n",
    "        'kernel': ['rbf'],\n",
    "        'class_weight': [None, 'balanced'],\n",
    "        'normalise': [True,False]\n",
    "    }\n",
    "]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.3 Either load or initialize our parameter search record\n",
    "\n",
    "Parameter searches can take a loooong time. This search record stores results to disk, so we can freely start and stop our search without loosing progress."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if not os.path.exists('tuning'):\n",
    "    os.mkdir('tuning')\n",
    "\n",
    "svc_search_record = sksk.ParameterSearchRecord('tuning/iris_svc_tuning.pkl')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.4 Run the parameter search\n",
    "\n",
    "In this example we have a budget of 1000, meaning that we will try 1000 random combinations of parameters, and keep the best one"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bestConf, bestScore = tuner.do_search(svc_search_params, svc_search_record)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('best score is {}, obtained with:'.format(bestScore))\n",
    "\n",
    "for (key, val) in bestConf.items():\n",
    "    print(\"  {} = {}\".format(key, val))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.5 Visualize parameter search\n",
    "\n",
    "It is often useful to visualize a parameter search, to get clues about how we might refine and do a followup search. It is common to start with a broad search and then narrow things down. \n",
    "\n",
    "We can use the `ParameterSearchVisualizer` to create heatmaps of how the gridsearch went.\n",
    "\n",
    "In this example we plot `gamma` and `C` while freezing the other variables. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "visualizer = sksk.ParameterSearchVisualizer()\n",
    "\n",
    "visualizer.visualize_grid_search(\n",
    "    svc_search_record, \n",
    "    x_axis='gamma', \n",
    "    y_axis='C', \n",
    "    filter_by={'kernel':'rbf', 'normalise':False, 'class_weight':'balanced'},\n",
    "    figsize=(8, 6)\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Evaluate the algorithm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3.1 Define our evaluator\n",
    "\n",
    "We need to define our evaluator class, in much the same way as we defined our parameter searcher"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class IrisEvaluator(sksk.Evaluator):\n",
    "    \n",
    "    def initialize_algorithm(self, config):\n",
    "        \n",
    "        return IrisClassifier(config)\n",
    "    \n",
    "evaluator = IrisEvaluator(items=data.data, labels=data.target)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3.2 Evaluate our algorithm\n",
    "\n",
    "The evaluator takes an array of configurations, and will print out how the algorithm performs with each configuration. In this case we will compare the our tuned algorithm (using the best params from our param search above) with the untuned one (using just default params). The evaluator also automatically adds a random baseline as an extra point of comparison."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "configs = []\n",
    "configs.append(sksk.merge_configs({'id':'tuned'}, bestConf))\n",
    "configs.append({'id':'untuned'})\n",
    "\n",
    "results = evaluator.evaluate(configs)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print_score_table(results)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The results are based on cross-fold validation, which gives slightly different results each time you run it. This lets us also provide pairwise t-tests to check whether the differences are statistically significant."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print_pairwise_comparisons(results)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we had more data, we would ideally have a proper training and testing split. The evaluator has a seperate method `evaluate_with_testset()` for this purpose. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3.3 Perform further evaluations and analysis\n",
    "\n",
    "This same process of feeding different configurations into the evaluator can be used for many other evaluations as well. \n",
    "\n",
    "For example, which is classification algorithm better for this problem (ignoring tuning for now)?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "configs = [\n",
    "    {'id':'SVM', 'algorithm':'SVC'},\n",
    "    {'id':'RandomForest', 'algorithm':'RandomForestClassifier'},\n",
    "    {'id':'GaussianNB', 'algorithm':'GaussianNB'}\n",
    "]\n",
    "\n",
    "results = evaluator.evaluate(configs)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print_score_table(results)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Or what is the individual contribution of each feature or feature group?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "configs = []\n",
    "\n",
    "configs.append(sksk.merge_configs({'id':'everything'}, bestConf))\n",
    "\n",
    "for feature in data.feature_names:\n",
    "    \n",
    "    config = {\n",
    "        'id': 'without {}'.format(feature),\n",
    "        'features': [f for f in data.feature_names if f != feature]\n",
    "    }\n",
    "    config = sksk.merge_configs(config, bestConf)\n",
    "    configs.append(config)\n",
    "    \n",
    "results = evaluator.evaluate(configs)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print_score_table(results)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "language_info": {
   "name": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
